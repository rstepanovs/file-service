package lv.romans.meters.files.controllers

import lv.romans.meters.commons.messaging.BillUploaded
import lv.romans.meters.commons.services.CanUserDownloadBill
import lv.romans.meters.files.api.VerificationApi
import org.springframework.amqp.core.AmqpTemplate
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.FileSystemResource
import org.springframework.core.io.Resource
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Service
import org.springframework.util.StringUtils
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.security.Principal
import java.text.SimpleDateFormat
import java.util.*

@RestController
class FileController(private val storage: Storage, private val verificationApi: VerificationApi) {


    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    @PostMapping("uploadBill")
    fun uploadBill(principal: Principal,
                   @RequestHeader("apartment") apartmentId: Long,
                   @RequestParam("file") file: MultipartFile) {
        storage.save(file, apartmentId, principal.name)
    }

    @PreAuthorize("hasAuthority('ROLE_USER')")
    @PostMapping("download/{file}")
    fun downloadBill(principal: Principal, @PathVariable("file") fileName: String): Resource {
        verificationApi.canDownloadBill(CanUserDownloadBill(fileName, principal.name))
        return storage.fetch(fileName)
    }

    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    @PostMapping("download/manager/{file}")
    fun downloadBill(@PathVariable("file") fileName: String): Resource {
        return storage.fetch(fileName)
    }

}

@Service
class Storage(@Value("\${billDirectory}") val dir: String, private val template: AmqpTemplate) {

    fun save(file: MultipartFile, apartmentId: Long, manager: String) {
        val currentDate = Date()
        val fileName = writeFile(currentDate, file)
        template.convertAndSend("bill_uploaded", BillUploaded(fileName, apartmentId, currentDate))
    }

    private fun writeFile(currentDate: Date, file: MultipartFile): String {
        val uploadDay = SimpleDateFormat("dd_MM_YY").format(currentDate)
        val originalFileName = StringUtils.cleanPath(file.originalFilename!!).removeSuffix(".pdf")
        val fileName = "$originalFileName-$uploadDay.pdf"
        val path = Paths.get("$dir${File.separator}$fileName.pdf")
        Files.copy(file.inputStream, path)
        return fileName
    }

    fun fetch(fileName: String): Resource {
        val file = Paths.get("$dir${File.separator}$fileName").toFile()
        return FileSystemResource(file)
    }

}