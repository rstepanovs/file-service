package lv.romans.meters.files.api

import lv.romans.meters.commons.services.CanUserDownloadBill
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestBody


@FeignClient("property-service")
interface VerificationApi {

    @GetMapping("/internal/verify")
    fun canDownloadBill(@RequestBody info: CanUserDownloadBill)
}