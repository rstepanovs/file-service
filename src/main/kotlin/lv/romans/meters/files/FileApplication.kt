package lv.romans.meters.files

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter
import org.springframework.amqp.support.converter.MessageConverter
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan

@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
@ComponentScan("lv.romans.meters")
class FileApplication {
    @Bean
    fun jsonMessageConverter(): MessageConverter = Jackson2JsonMessageConverter(ObjectMapper().apply { registerModule(KotlinModule()) })
}

fun main(args: Array<String>) {
    runApplication<FileApplication>(*args)
}
